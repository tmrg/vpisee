/*
 *  Pedro.Leitao@cern.ch 2020
 *
*/

`timescale 1ps/1ps

module harness();


`ifdef SEE
  // - - - - - - - - - - - - Single Event Effect section - - - - - - - - - - - -
  import "DPI-C" function void seeg(string scope,string ff_seu_set, int ff_seu_set_value, string ff_seu_reset, int ff_seu_reset_value);
  import "DPI-C" function void print_set_map();
  
  import "DPI-C" function void flip_random_reg(int seucounter);
  import "DPI-C" function void unflip_last_reg(int seucounter, int seeduration);

  import "DPI-C" function void flip_random_net(int seucounter);
  import "DPI-C" function void unflip_last_net(int seucounter, int seeduration);

  string scope        = "harness.DUT"; // can be even lower in the hierarchy 
  string ff_seu_set   = "S_see"; 
  string ff_seu_reset = "R_see";
  
  int ff_seu_set_value    = 1;
  int ff_seu_reset_value  = 1;
  
  reg     SEEEnable=0;          // enables SEE generator
  reg     SEEActive=0;          // high during any SEE event
  integer SEEnextTime=0;        // time until the next SEE event
  integer SEEduration=0;        // duration of the next SEE event
  integer SEEwireId=0;          // wire to be affected by the next SEE event
  integer SEEmaxWireId=0;       // number of wires in the design which can be affected by SEE event
  integer SEEmaxUpaseTime=1000; // 1 ns  (change if you are using different timescale)
  integer SEEDel=100_000;       // 100 ns (change if you are using different timescale)
  integer SEECounter=0;         // number of simulated SEE events
  integer SEEduration_factor=3;
  reg     SEErandom_set=0;

  real SEEActivetime_pre;             initial SEEActivetime_pre = 0;
  real SEEParticuleFrequency_MHz;     initial SEEParticuleFrequency_MHz     = 0;
  real SEEParticuleFrequency_MHz_max; initial SEEParticuleFrequency_MHz_max = 0;
  real SEEParticuleFrequency_MHz_min; initial SEEParticuleFrequency_MHz_min = 999999;

  initial begin
    seeg(scope, ff_seu_set, ff_seu_set_value, ff_seu_reset,ff_seu_reset_value);
    print_set_map();
  end

  initial begin
    // enable SEE after 1ms
    #1000_000 SEEEnable=1;
    $display("Enabling SEE generation");
  end

  always
    begin
      if (SEEEnable)
        begin
          // randomize time, duration, and wire of the next SEE
          SEEnextTime = #(SEEDel/2) {$random} % SEEDel;
          SEEduration = SEEduration_factor * ({$random} % (SEEmaxUpaseTime-1) + 1);  // SEE time is from 1 - MAX_UPSET_TIME ns
          //see_random_wireid(SEEwireId); //{$random} % SEEmaxWireId;

          // wait for SEE
          #(SEEnextTime);

          // SEE happens here! Toggle the selected wire.
          SEECounter=SEECounter+1;
          SEEActive=1;
          SEEParticuleFrequency_MHz = 1/($time - SEEActivetime_pre)*1e6;
		  SEEParticuleFrequency_MHz_max = SEEParticuleFrequency_MHz_max < SEEParticuleFrequency_MHz ? SEEParticuleFrequency_MHz : SEEParticuleFrequency_MHz_max;
		  SEEParticuleFrequency_MHz_min = SEEParticuleFrequency_MHz_min > SEEParticuleFrequency_MHz ? SEEParticuleFrequency_MHz : SEEParticuleFrequency_MHz_min;
		  SEEActivetime_pre = $time;
          SEErandom_set={$random};
          if(SEErandom_set) begin
            flip_random_reg(SEECounter);
            #(SEEduration);
            unflip_last_reg(SEECounter,SEEduration);		  
		  end else begin
            flip_random_net(SEECounter);
            #(SEEduration);
            unflip_last_net(SEECounter,SEEduration);		  
		  end
          SEEActive=0;
        end
      else
        #1_000_000;
    end
`endif

//initial begin
//  $sdf_annotate(`getstring(`ABCSTAR_SDF), harness.DUT, ,"DUT.sdf.log", "TYPICAL");
//end

reg clk;
reg rst;
always begin
  clk = 0;
  #1000 clk =1;
  forever #25_000 clk = ~clk;
end

initial begin
  rst = 1;
  #1_000_000;
  rst = 0;
end

rippleCounter DUT(
  .clk(clk),
  .rst(rst),
  .count()
  );
endmodule

module rippleCounter (clk,rst,count);
parameter SIZE = 4;
input clk;
input rst;
output [SIZE-1:0] count;

wire [SIZE  :0] clkint;
wire [SIZE-1:0] qbarint;
wire [SIZE-1:0] qbarintbuf;

assign clkint[0] = clk;

/* // cannot really used genvar with the current vpisee
genvar i;
generate 
  for(i=0; i<SIZE; i=i+1) begin
    pl2020_ffd    ffd_inst(.Q(clkint[i+1]),.QBAR(qbarint[i]),.CLK(clkint[i]),.D(qbarintbuf[i]), .R(rst));
    pl2020_buffer buf_inst(.A(qbarint[i]),.Z(qbarintbuf[i]));
  end
endgenerate  
*/

    pl2020_ffd    ffd_inst0(.Q( clkint[1]),.QBAR(qbarint[0]),.CLK(clkint[0]),.D(qbarintbuf[0]), .R(rst));
    pl2020_buffer buf_inst0(.A(qbarint[0]),.Z(qbarintbuf[0]));

    pl2020_ffd    ffd_inst1(.Q( clkint[2]),.QBAR(qbarint[1]),.CLK(clkint[1]),.D(qbarintbuf[1]), .R(rst));
    pl2020_buffer buf_inst1(.A(qbarint[1]),.Z(qbarintbuf[1]));

    pl2020_ffd    ffd_inst2(.Q( clkint[3]),.QBAR(qbarint[2]),.CLK(clkint[2]),.D(qbarintbuf[2]), .R(rst));
    pl2020_buffer buf_inst2(.A(qbarint[2]),.Z(qbarintbuf[2]));

    pl2020_ffd    ffd_inst3(.Q( clkint[4]),.QBAR(qbarint[3]),.CLK(clkint[3]),.D(qbarintbuf[3]), .R(rst));
    pl2020_buffer buf_inst3(.A(qbarint[3]),.Z(qbarintbuf[3]));

assign count = qbarintbuf;

endmodule


module pl2020_ffd(Q,QBAR,CLK,D,R);
input CLK;
input D;
input R;
output Q;
output QBAR;

reg Q, QBAR;

//reg R;      initial R = 0;
reg R_init; initial R_init = 0;
reg R_see;  initial R_see = 0;

reg S; initial S = 0;
reg S_init; initial S_init = 0;
reg S_see;  initial S_see = 0;

initial begin 
  if($random) R_init = 1; 
  else S_init = 1;   
  #10_000;
  R_init = 0; S_init = 0;
end

reg S_int; assign S_int = S|S_init|S_see;
reg R_int; assign R_int = R|R_init|R_see;

always @(posedge CLK or posedge S_int or posedge R_int) begin 
  if(R|R_init|R_see) begin
    Q    <= #1  0;
    QBAR <= #1 ~0;  
  end else if (S|S_init|S_see) begin
    Q    <= #1  1;
    QBAR <= #1 ~1;  
  end else begin
    Q    <= #1  D;
    QBAR <= #1 ~D;
  end
end

endmodule

module pl2020_buffer(A,Z);
input A;
output Z;

assign #(1:2:3) Z = A;

endmodule
