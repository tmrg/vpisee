/****************************************************************************************************************
// Pedro.Leitao@cern.ch
// Starting point: https://stackoverflow.com/questions/49138155/obtaining-signal-names-in-the-design-using-vpi-calls
// So far this should not be used with RTL

//1. get all all registers and look for net  .RN_i and S_i

 ****************************************************************************************************************/
#include "vhpi_user.h"
#include "vpi_user.h"
#include "vpi_user_cds.h"
#include "sv_vpi_user.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unordered_map>
#include <regex>

extern "C" {

#define check_verilog(scopeH)  (vpi_get(vpiLanguage,scopeH) == vpiVerilog)
#define check_is_protected(scopeH)  (vpi_get(vpiIsProtected,scopeH))
#define check_protected(scopeH)  (vpi_get(vpiProtected,scopeH))
#define check_vhdl(scopeH)  (vpi_get(vpiLanguage,scopeH) == vpiVHDL)


static std::unordered_map<int , std::string> reg_map;  // wireid, { wirename, default_state:0->S_i,1->RN_i}
static std::unordered_map<int , std::string> net_map;

static std::string ff_seu_set;
static std::string ff_seu_reset;

static int ff_seu_set_value;
static int ff_seu_reset_value;


static int reg_map_flipped_index;
static int net_map_flipped_index;


static s_vpi_value net_value_before_set;

// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool is_valid_scope(vpiHandle scopeH) {
  switch (vpi_get(vpiType, scopeH)) {
    case vpiInstance:
    case vpiModule:
    case vpiGenScope:
    case vpiGenScopeArray:
    case vpiInstanceArray:
    return true;
    default:
    return false;
  }
}

void print_set_map() {
  for (auto const& x : reg_map) {
    vpi_printf("[r%d/%d]\t %s \n",x.first,reg_map.size(),x.second.c_str());
  }
  for (auto const& x : net_map) {
    vpi_printf("[o%d/%d]\t %s \n",x.first,net_map.size(),x.second.c_str());
  }  
}

int vpi_flip_bit(vpiHandle reg, const int mapindex, const int seucounter) {
  int regWidth = vpi_get(vpiSize, reg);
  int flipPosition = rand()%regWidth;

  vpiHandle reg_bit;
  if (regWidth > 1)
    reg_bit = vpi_handle_by_index(reg,flipPosition);
  else
    reg_bit = reg;

  std::string reg_name(vpi_get_str(vpiFullLSName, reg_bit));
  s_vpi_value value;   value.format = vpiIntVal;

  if(reg_name.compare(reg_name.size()-ff_seu_set.size(), ff_seu_set.size(),ff_seu_set )==0 ) { // set
    if(ff_seu_set_value == 1)    value.value.integer = 1;
    if(ff_seu_set_value == 0)    value.value.integer = 0;

  } else if(reg_name.compare(reg_name.size()-ff_seu_reset.size(), ff_seu_reset.size(),ff_seu_reset )==0 ) { // seu
    if(ff_seu_reset_value == 1)    value.value.integer = 1;
    if(ff_seu_reset_value == 0)    value.value.integer = 0;
	
  } else {
    vpi_printf ("*E flipping wrong register");
    return 1;
  }
    vpi_printf ("[%d]   Flipping bit [r%d/%d] in  %s \n",seucounter,mapindex,reg_map.size(),reg_name.c_str());
    vpi_put_value(reg_bit,&value,NULL,vpiForceFlag);
    
  return 0;
}

int vpi_flip_net(vpiHandle net, const int mapindex, const int seucounter) {

  std::string net_name(vpi_get_str(vpiFullLSName, net));
  
  // get value of net before SEU
  vpi_get_value(net,&net_value_before_set); //Getting the initial value of the net

  s_vpi_value net_value_set;  net_value_set.format = vpiIntVal;
  net_value_set.value.integer = net_value_before_set.value.integer ^ 1; //Flipping the bit

  //Forcing the value at a given position.
  vpi_printf ("[%d]   Flipping net [o%d/%d] in  %s \n",seucounter,mapindex,net_map.size(),net_name.c_str());
  vpi_put_value(net,&net_value_set,NULL,vpiForceFlag);
    
  return 0;
}

int vpi_unflip_bit(vpiHandle reg, const int mapindex, const int seucounter, const int seeduration) {
  int regWidth = vpi_get(vpiSize, reg);
  int flipPosition = rand()%regWidth;

  vpiHandle reg_bit;
  if (regWidth > 1)
    reg_bit = vpi_handle_by_index(reg,flipPosition);
  else
    reg_bit = reg;

  std::string reg_name(vpi_get_str(vpiFullLSName, reg_bit));
  s_vpi_value value;   value.format = vpiIntVal;
  
  if(reg_name.compare(reg_name.size()-ff_seu_set.size(), ff_seu_set.size(),ff_seu_set )==0 ) { // set
    if(ff_seu_set_value == 1)    value.value.integer = 0;
    if(ff_seu_set_value == 0)    value.value.integer = 1;

  } else if(reg_name.compare(reg_name.size()-ff_seu_reset.size(), ff_seu_reset.size(),ff_seu_reset )==0 ) { // seu
    if(ff_seu_reset_value == 1)    value.value.integer = 0;
    if(ff_seu_reset_value == 0)    value.value.integer = 1;
  } else {
    vpi_printf ("*E Unflipping wrong register");
    return 1;
  }
  
    vpi_printf ("[%d] Unflipping bit [r%d/%d] in  %s after %d ps\n",seucounter,mapindex,reg_map.size(),reg_name.c_str(),seeduration);
    vpi_put_value(reg_bit,&value,NULL,vpiForceFlag);
    
  return 0;
}
int vpi_unflip_net(vpiHandle net, const int mapindex, const int seucounter, const int seeduration) {

  std::string net_name(vpi_get_str(vpiFullLSName, net));
  
  //Forcing the value at a given position.
  vpi_printf ("[%d] Unflipping net [o%d/%d] in  %s after %d ps\n",seucounter,mapindex,net_map.size(),net_name.c_str(),seeduration);
//  vpi_put_value(net,&net_value_before_set,NULL,vpiForceFlag);
  s_vpi_value return_value;   return_value.format = vpiIntVal;

  vpi_put_value(net,&net_value_before_set,NULL,vpiReleaseFlag);
  
    
  return 0;
}

void flip_reg(const char * reg_name,const int mapindex,  const int seucounter) {
  vpiHandle reg;
  reg = vpi_handle_by_name((PLI_BYTE8 *)reg_name,NULL);
  vpi_flip_bit(reg,mapindex,seucounter);
}
void flip_net(const char * net_name,const int mapindex,  const int seucounter) {
  vpiHandle net;
  net = vpi_handle_by_name((PLI_BYTE8 *)net_name,NULL);
  vpi_flip_net(net,mapindex,seucounter);
}

void unflip_reg(const char * reg_name,const int mapindex,  const int seucounter, const int seeduration) {
  vpiHandle reg;
  reg = vpi_handle_by_name((PLI_BYTE8 *)reg_name,NULL);
  vpi_unflip_bit(reg,mapindex,seucounter,seeduration);
}
void unflip_net(const char * net_name,const int mapindex,  const int seucounter, const int seeduration) {
  vpiHandle net;
  net = vpi_handle_by_name((PLI_BYTE8 *)net_name,NULL);
  vpi_unflip_net(net,mapindex,seucounter,seeduration);
}

void flip_random_reg(const int seucounter) {
  if (reg_map.size() != 0) {
    int regIndex = rand()%reg_map.size();
    reg_map_flipped_index = regIndex;
    //vpi_printf("*I flipping mapid[%d/%d]\n", regIndex,reg_map.size());
	if(regIndex<reg_map.size()) {
    flip_reg(reg_map[regIndex].c_str(),reg_map_flipped_index,seucounter);
	} else {
    vpi_printf("*I,E flipping mapid[%d/%d] out of specs\n", regIndex,reg_map.size());
	}
  }
  else
    vpi_printf("Can't flip register because the register map is empty \n");
}

void flip_random_net(const int seucounter) {
  if (net_map.size() != 0) {
    int netIndex = rand()%net_map.size();
    net_map_flipped_index = netIndex;
	if(netIndex<net_map.size()) {
    flip_net(net_map[netIndex].c_str(),net_map_flipped_index,seucounter);
	} else {
    vpi_printf("*I,E flipping mapid[o%d/%d] out of specs\n", netIndex,net_map.size());
	}
  }
  else
    vpi_printf("Can't flip net because the output map is empty \n");
}


void unflip_last_reg(const int seucounter, const int seeduration) {
  if (reg_map.size() != 0) {
  	if(reg_map_flipped_index<reg_map.size()) {
    unflip_reg(reg_map[reg_map_flipped_index].c_str(),reg_map_flipped_index,seucounter,seeduration);
	} else {
	    vpi_printf("*I,E unflipping mapid[r%d/%d] out of specs\n", reg_map_flipped_index,reg_map.size());
	}
  }
  else
    vpi_printf("Can't flip register because the register map is empty \n");
}

void unflip_last_net(const int seucounter, const int seeduration) {
  if (net_map.size() != 0) {
  	if(net_map_flipped_index<net_map.size()) {
    unflip_net(net_map[net_map_flipped_index].c_str(),net_map_flipped_index,seucounter,seeduration);
	} else {
	    vpi_printf("*I,E unflipping mapid[o%d/%d] out of specs\n", net_map_flipped_index,net_map.size());
	}
  }
  else
    vpi_printf("Can't flip register because the register map is empty \n");
}

static int getExprValue(vpiHandle conn, int r) {
    vpiHandle expr = vpi_handle(r, conn);

    s_vpi_value val = {vpiIntVal};
    vpi_get_value(expr, &val);
    return val.value.integer;
}

void vpi_get_outpin(vpiHandle module) {
  vpiHandle portItr , portHndl, netHndl;
  char dirn[8];
  
  int leftaddr;
  int rightaddr;
  
  int addrindex, addrstart, addrstop;
  
   vpiHandle highConn;
   vpiHandle lowConn ;
  
  if(vpi_get(vpiType, module) == vpiGenScope) return;
  
  if ((portItr = vpi_iterate(vpiPort,module))) {

    if (!portItr) return;
	
    while ((portHndl = vpi_scan(portItr))) {
	  
	  if(vpi_get(vpiDirection,portHndl) == vpiOutput) { // we only care about outputs
	  
        lowConn  = vpi_handle(vpiLowConn,  portHndl);

        if(lowConn == NULL) {
        } else {
		

          if(vpi_get(vpiSize, lowConn) > 1) {
		    leftaddr = getExprValue(lowConn, vpiLeftRange);
			rightaddr = getExprValue(lowConn, vpiRightRange);
			
			if(leftaddr<rightaddr) {
			  addrstart = leftaddr;
			  addrstop  = rightaddr;
			} else {
			  addrstart = rightaddr;
			  addrstop  = leftaddr;
			}
			for(addrindex = addrstart; addrindex <= addrstop; addrindex = addrindex + 1) {
			   net_map[(int)net_map.size()] = vpi_get_str(vpiFullLSName, vpi_handle_by_index(lowConn,addrindex));
			}
          } else {
             net_map[(int)net_map.size()] = vpi_get_str(vpiFullLSName, lowConn);

		  }
		}

	  
	  } else {
	  }
	}
  }
}

void vpi_get_reg(vpiHandle module) {
  vpiHandle itr_reg, reg;
  if ((itr_reg = vpi_iterate(vpiReg,module))) {
    while ((reg = vpi_scan(itr_reg))) {
      std::string reg_name(vpi_get_str(vpiFullLSName, reg));
    /*
      vpi_printf("********************************************\n");
      vpi_printf("** reg_name.size() = %d\n",reg_name.size());
      vpi_printf("** ff_seu_set.size() = %d\n",ff_seu_set.size());
      vpi_printf("** ff_seu_reset.size() = %d\n",ff_seu_reset.size());

      vpi_printf("** reg_name.compare(reg_name.size()-ff_seu_set.size(), ff_seu_set.size(),ff_seu_set )       = %d\n",reg_name.compare(reg_name.size()-ff_seu_set.size(), ff_seu_set.size(),ff_seu_set ));
      vpi_printf("** reg_name.compare(reg_name.size()-ff_seu_reset.size(), ff_seu_reset.size(),ff_seu_reset ) = %d\n",reg_name.compare(reg_name.size()-ff_seu_reset.size(), ff_seu_reset.size(),ff_seu_reset ));
	*/	

      if(        reg_name.compare(reg_name.size()-ff_seu_set.size(), ff_seu_set.size(),ff_seu_set )==0 ) { // set
        //vpi_printf("** Verilog register Full Name:\t%s[%d] : set-OK\n",reg_name.c_str(),vpi_get(vpiSize, reg));
        reg_map[(int)reg_map.size()] = reg_name;

      } else if(reg_name.compare(reg_name.size()-ff_seu_reset.size(), ff_seu_reset.size(),ff_seu_reset )==0 ) { // seu
        //vpi_printf("** Verilog register Full Name:\t%s[%d] : reset-OK\n",reg_name.c_str(),vpi_get(vpiSize, reg));
        reg_map[(int)reg_map.size()] = reg_name;

      } else {
        //vpi_printf("** Verilog register Full Name:\t%s[%d]\n",reg_name.c_str(),vpi_get(vpiSize, reg));
      }
      //vpi_printf("********************************************\n");

    }
  }
}

void walk_down(vpiHandle parentScope) {
  vpiHandle subScopeI, subScopeH;
  
//  vpi_printf("** Walking down ... vpiFullLSName: %s ; vpiName: %s ; vpiDefName: %s; vpiType: %s\n",vpi_get_str(vpiFullLSName, parentScope),  vpi_get_str(vpiName, parentScope),  vpi_get_str(vpiDefName, parentScope), vpi_get_str(vpiType, parentScope));
  
  if (check_verilog(parentScope) && is_valid_scope(parentScope)) {
      vpi_get_reg(parentScope);
	  
    if ((subScopeI = vpi_iterate(vpiInternalScope, parentScope))) { // if we can go down, go down
      while ((subScopeH = vpi_scan(subScopeI))) {
          walk_down(subScopeH);
      }
    } else {
	  //vpi_printf("  && vpi_iterate(vpiInternalScope, parentScope) returned false\n");
      vpi_get_outpin(parentScope);

	}	
  } else {
  //vpi_printf("  && check_verilog(parentScope) && is_valid_scope(parentScope) returned false\n");
  
  }  
  //vpi_printf("** Walking up ... %s : %s : %s\n",vpi_get_str(vpiFullLSName, parentScope),  vpi_get_str(vpiName, parentScope),  vpi_get_str(vpiDefName, parentScope));
}


void seeg(const char * scope, const char * ff_set, const int ff_set_value, const char * ff_reset, const int ff_reset_value) {
  reg_map.clear();
  net_map.clear();
  
  net_value_before_set.format = vpiIntVal;

  
  ff_seu_set    = ff_set;
  ff_seu_reset  = ff_reset;
  ff_seu_set_value   = ff_set_value;
  ff_seu_reset_value = ff_reset_value;
  
  vpiHandle topScopeI, topScopeH;
  
//  vpi_printf(".........Starting register discovery \n");
  if ((topScopeH = vpi_handle_by_name((PLI_BYTE8 *)scope, NULL))) {
    topScopeI = vpi_iterate(vpiModule, topScopeH);
    while ((topScopeH = vpi_scan(topScopeI))) {
      walk_down(topScopeH);
	}
  }
//  vpi_printf("Completed register discovery........\n");
}

// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
