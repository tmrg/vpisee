# Pedro.Leitao@cern.ch 2020
###############################################################################
MAKEFILE_ROOT_GIT = $(shell git rev-parse --show-toplevel)
###############################################################################
currentDate                 = $(shell date +%Y%m%d_%H%M%S)
workdir                     = work
SIMCONTROL_SIMULATOR        = xrun
SIMCONTROL_REFLIBDIR        = xcelium.d
SIMCONTROL_EXTRAOPTIONS     = 
SIMCONTROL_SEE              = 
###############################################################################
SEE_DEFINES =
ifeq ($(SIMCONTROL_SEE), SEE)
	SEE_DEFINES += -define SEE
endif
###############################################################################
TESTVPI_CFUNCTIONS = \
	$(MAKEFILE_ROOT_GIT)/src/vpi_c/seeg.cpp
###############################################################################

# targets
###############################################################################
help:
	@echo ""
	@echo "======================================================"
	@echo "MAKEFILE_ROOT_GIT = $(MAKEFILE_ROOT_GIT)"
	@echo "======================================================"
	@echo "-help? check Makefile"
	@echo ""
###############################################################################
example:
	@echo ""
	@echo "======================================================"
	@echo "MAKEFILE_ROOT_GIT = $(MAKEFILE_ROOT_GIT)"
	@echo "======================================================"
	@echo ""
	@rm -r $(workdir) || true
	@mkdir -p $(workdir)
	@cd  $(workdir) ; \
	touch empty_cds.lib ; \
	$(SIMCONTROL_SIMULATOR)  \
		-cdslib empty_cds.lib \
		-access +rwc \
		-sv \
		-gui \
		$(SEE_DEFINES) \
		$(SIMCONTROL_EXTRAOPTIONS) \
		$(TESTVPI_CFUNCTIONS) \
		$(MAKEFILE_ROOT_GIT)/src/example/testbench.v
