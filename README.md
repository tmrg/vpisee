# vpisee

A description of how to inject Single Event Effecsts (= Single Event Upsets + Single Event Transient) in a synthesized/place&route netlist

## quickstart

simply run `make example`

## Licenses
This software is distributed under the terms of the GPL Version 2 license. A copy of this license can be found in [LICENSE.md](LICENSE.md).

## Contributors

* Pedro Leitao, CERN, EP/ESE/ME (@pevicent)
* Omer Can Akgun, NIKHEF (@oakgun)

## Contributing

All types of contributions, being it minor and major, are very welcome.
